function getId(id) {
    return document.getElementById(id);
}
var list = [];
var elements = {
    userInput: getId('userInput'),
    addButton: getId('addButton'),
    result: getId('result'),
    alert: document.getElementById('alert'),
}
var string = {
    empty: "Please Enter Your Name ?!!!",
    duplicate: "Enter a New Name",
    success: "Your Name Changed"
}
function showAlert(message) {
    elements.alert.innerHTML = message
    elements.alert.style.display = "block"
}

function addName(e) {
    e.preventDefault();
    if (!elements.userInput.value) {
        showAlert(string.empty);
    } else {
        list.push(elements.userInput.value);
        elements.alert.style.display = "none"
    }
    elements.userInput.value = ""
    var showResult = list.reduce(function (total, item, index, array) {

        total += ` <li class = "generated-form"> <input value = "${item}" id = "newItem" class = "res-input">
                    <button id = "editButton" class = "edit-button" onclick = "editItems(${index})">Edit</button> 
                    </li>`

        return total
    }, "<ul>")
    elements.result.innerHTML = showResult
}
elements.addButton.addEventListener('click', addName);

function editItems(index) {
    var newInput = [...document.getElementsByClassName('res-input')];
    var newItem = newInput[index].value;
    list = list.map(function (item, i) {
        if (i === index) {

            return newItem


        } else {
            return item;
        }

    })
    console.log(list);
    showAlert(string.success);
    elements.alert.style.backgroundColor = "green"
};
