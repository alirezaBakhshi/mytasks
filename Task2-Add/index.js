function getId(id){
    return document.getElementById(id);
}
var list = [];
var elements = {
    userInput : getId('userInput') , 
    addButton : getId('addButton') , 
    result : getId('result') ,
    resetButton : getId('resetButton'),
    alert : getId('alert'),
};
var strings = {
    empty : "Please Enter Your Name",
}
function showAlert(message){
    elements.alert.innerHTML = message;
    elements.alert.style.display = "block"
}
function addTodo (e){
    e.preventDefault()
    if (!elements.userInput.value){
        showAlert(strings.empty)
    }else {
    list.push(elements.userInput.value);
    elements.alert.style.display = "none"
    elements.userInput.value = ""
    console.log(list)
    }
    var showList = list.reduce(function(total , item){
        total += `<li class = "li">${item}</li>`;
        return total;
    },"<ul>");
    elements.result.innerHTML = showList
}
elements.addButton.addEventListener('click' , addTodo);
elements.resetButton.addEventListener('click' ,reset)
function reset(e){
    e.preventDefault()
    list = [];
    console.log(list);
    elements.result.innerHTML = "";
}