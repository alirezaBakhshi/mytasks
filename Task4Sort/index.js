function getId(id) {
    return document.getElementById(id);
}
var elements = {
    userName: getId('userName'),
    userFamily: getId('userFamily'),
    userAge: getId('userAge'),
    userHeight: getId('userHeight'),
    submitButton: getId('submitButton'),
    alert: getId('alert'),
    olderButton: getId('olderPerson'),
    shorterPerson: getId('shorterPerson'),
    result: getId('result'),
}
var strings = {
    empty: "Please Fill All Inputs"
}
function showAlert(message) {
    elements.alert.style.display = "block";
    elements.alert.innerHTML = message;
}
var person = [
    {
        name: "Alireza",
        family: "Bakhshi",
        age: "20",
        height: "176",
    }
]
function addPerson(e) {
    e.preventDefault();
    if (!elements.userName.value || !elements.userFamily.value || !elements.userAge.value || !elements.userHeight.value) {
        showAlert(strings.empty);
    } else {
        elements.alert.style.display = "none"
        person.push({
            name: elements.userName.value,
            family: elements.userFamily.value,
            age: elements.userAge.value,
            height: elements.userHeight.value,
        });
        console.log(person);
        emptyValue(elements.userName ,elements.userFamily,elements.userAge , elements.userHeight);
        var showList = person.reduce(function (total, item, ) {
            total += `<li class = "li">${item.name + " " + item.family + " " + item.age + " " + item.height}</li>`;

            return total;
        }, "<ul>");
        elements.result.innerHTML = showList;
    }
}
elements.submitButton.addEventListener('click', addPerson);
function findOlder(e) {
    e.preventDefault();
    var getAge = person.sort(function (a, b) { return b.age - a.age });
    var showOlder = "<p class = 'li'>" + getAge[0].name + " " + getAge[0].family + " " + " is " + getAge[0].age + " " + "Years Old" + "</p>"
    elements.result.innerHTML = showOlder
}
elements.olderButton.addEventListener('click', findOlder);
function emptyValue(one,two,three,four) {
    one.value = "";
    two.value = "";
    three.value = "";
    four.value = "";

}elements.shorterPerson.addEventListener('click' , findShorter)
function findShorter (e){
    e.preventDefault();
    var height = person.sort(function (a, b) { return a.height - b.height });
    var showHeight = "<p class = 'li'>" + height[0].name + " " + height[0].family + " " + " Height Is" +  height[0].height  + "</p>"
    elements.result.innerHTML = showHeight;
}